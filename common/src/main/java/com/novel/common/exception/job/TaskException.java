package com.novel.common.exception.job;

import com.novel.common.exception.base.BaseException;

import java.io.Serial;

/**
 * 计划策略异常
 *
 * @author novel
 * @date 2020/3/2
 */
public class TaskException extends BaseException {
    @Serial
    private static final long serialVersionUID = 1L;

    private Code errorCode;

    public TaskException(String module, String code, Object[] args, String defaultMessage) {
        super(module, code, args, defaultMessage);
    }

    public TaskException(String module, String code, Object[] args) {
        super(module, code, args);
    }

    public TaskException(String module, String defaultMessage) {
        super(module, defaultMessage);
    }

    public TaskException(String code, Object[] args) {
        super(code, args);
    }

    public TaskException(String defaultMessage) {
        super(defaultMessage);
    }

    public Code getErrorCode() {
        return errorCode;
    }

    public TaskException(String defaultMessage, Code errorCode) {
        super(defaultMessage);
        this.errorCode = errorCode;
    }


    public enum Code {
        /**
         * 任务存在
         */
        TASK_EXISTS,
        /**
         * 任务不存在
         */
        NO_TASK_EXISTS,
        /**
         * 任务已经启动
         */
        TASK_ALREADY_STARTED,
        /**
         * 未知
         */
        UNKNOWN,
        /**
         * 配置错误
         */
        CONFIG_ERROR,
        /**
         * 任务节点不可用
         */
        TASK_NODE_NOT_AVAILABLE
    }
}