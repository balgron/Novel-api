package com.novel.common.exception.gen;

import com.novel.common.exception.base.BaseException;

import java.io.Serial;

/**
 * 代码生成异常
 *
 * @author novel
 * @date 2021/5/7 20:19
 */
public class GenException extends BaseException {
    @Serial
    private static final long serialVersionUID = 1L;

    public GenException(String module, String code, Object[] args, String defaultMessage) {
        super(module, code, args, defaultMessage);
    }

    public GenException(String module, String code, Object[] args) {
        super(module, code, args);
    }

    public GenException(String module, String defaultMessage) {
        super(module, defaultMessage);
    }

    public GenException(String code, Object[] args) {
        super(code, args);
    }

    public GenException(String defaultMessage) {
        super(defaultMessage);
    }
}
