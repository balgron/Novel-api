package com.novel.resource.qiniu;

import com.novel.resource.qiniu.config.QiniuConfig;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.UploadManager;
import com.qiniu.util.Auth;

/**
 * 七牛云操作客户端
 *
 * @author novel
 * @date 2020/4/1
 */
public class QiniuClient {
    /**
     * 配置
     */
    private final QiniuConfig qiniuConfig;

    /**
     * 七牛云配置
     */
    private final Configuration cfg;
    /**
     * 上传管理器
     */
    private final UploadManager uploadManager;

    /**
     * 空间管理器
     */
    private final BucketManager bucketManager;

    /**
     * auth
     */
    private final Auth auth;

    /**
     * uploadToken
     */
    private final String uploadToken;

    public QiniuClient(QiniuConfig qiniuConfig) {
        this.qiniuConfig = qiniuConfig;
        this.auth = Auth.create(this.qiniuConfig.getQiniuAccessKey(), this.qiniuConfig.getQiniuSecretKey());
        uploadToken = this.auth.uploadToken(qiniuConfig.getBucketName());
        cfg = new Configuration(this.qiniuConfig.getRegion().getRegion());
        uploadManager = new UploadManager(cfg);
        bucketManager = new BucketManager(auth, cfg);
    }


    public QiniuConfig getQiniuConfig() {
        return qiniuConfig;
    }

    public Configuration getCfg() {
        return cfg;
    }

    public UploadManager getUploadManager() {
        return uploadManager;
    }

    public BucketManager getBucketManager() {
        return bucketManager;
    }

    public Auth getAuth() {
        return auth;
    }

    public String getUploadToken() {
        return uploadToken;
    }

    /**
     * 关闭
     */
    public void shutdown() {
    }
}
