package com.novel.resource.qiniu.impl;


import com.novel.common.resource.ResourceType;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

/**
 * qiniu 注入控制
 *
 * @author novel
 * @date 2020/3/31
 */
public class QiniuConditional implements Condition {
    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        ResourceType type = context.getEnvironment().getProperty("resource.file-type", ResourceType.class);
        return ResourceType.QI_NIU.equals(type);
    }
}
