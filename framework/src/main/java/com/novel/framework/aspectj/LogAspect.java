package com.novel.framework.aspectj;

import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson2.JSON;
import com.novel.common.exception.base.BaseException;
import com.novel.common.utils.StringUtils;
import com.novel.framework.annotation.Log;
import com.novel.framework.enums.BusinessStatus;
import com.novel.framework.shiro.utils.ShiroUtils;
import com.novel.framework.utils.AddressUtils;
import com.novel.framework.utils.servlet.IpUtils;
import com.novel.framework.utils.servlet.ServletUtils;
import com.novel.system.domain.SysDept;
import com.novel.system.domain.SysOperLog;
import com.novel.system.domain.SysUser;
import com.novel.system.service.SysDeptService;
import com.novel.system.service.SysOperLogService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Map;
import java.util.Set;

/**
 * 操作日志记录处理
 *
 * @author novel
 * @date 2019/5/14
 */
@Aspect
@Component
public class LogAspect {
    private static final Logger log = LoggerFactory.getLogger(LogAspect.class);

    private final SysOperLogService sysOperLogService;
    private final SysDeptService sysDeptService;

    public LogAspect(SysOperLogService sysOperLogService, SysDeptService sysDeptService) {
        this.sysOperLogService = sysOperLogService;
        this.sysDeptService = sysDeptService;
    }

    /**
     * 配置织入点
     */
    @Pointcut("@annotation(com.novel.framework.annotation.Log)")
    public void logPointCut() {
    }

    /**
     * 环绕通知 @Around  ， 当然也可以使用 @Before (前置通知)  @After (后置通知)
     *
     * @param point point
     * @return 拦截方法的返回值
     * @throws Throwable 异常
     */
    @Around("logPointCut()")
    public Object around(ProceedingJoinPoint point) throws Throwable {
        Throwable throwable = null;
        long time = 0;
        Object result = null;
        try {
            long beginTime = System.currentTimeMillis();
            result = point.proceed();
            time = System.currentTimeMillis() - beginTime;
        } catch (Throwable e) {
            throwable = e;
            throw e;
        } finally {
            handleLog(point, throwable, result, time);
        }
        return result;
    }


    private void handleLog(final JoinPoint joinPoint, final Throwable e, Object jsonResult, Long processingTime) {
        try {
            // 获得注解
            Log controllerLog = getAnnotationLog(joinPoint);
            if (controllerLog == null) {
                return;
            }

            // 获取当前的用户
            SysUser currentUser = ShiroUtils.getUser();
            // *========数据库日志=========*//
            SysOperLog operLog = new SysOperLog();
            operLog.setStatus(BusinessStatus.SUCCESS.ordinal());
            operLog.setProcessingTime(processingTime);
            // 请求的地址
            HttpServletRequest request = ServletUtils.getRequest();
            String ipAddr = IpUtils.getIpAddr(request);
            operLog.setOperIp(ipAddr);
            // 返回参数
            operLog.setJsonResult(ObjectUtil.isNotNull(jsonResult) ? JSON.toJSONString(jsonResult) : "{}");

            operLog.setOperUrl(ServletUtils.getRequest().getRequestURI());

            operLog.setOperName(currentUser.getUserName());
            if (e != null) {
                operLog.setStatus(BusinessStatus.FAIL.ordinal());
                if (e instanceof BaseException) {
                    operLog.setErrorMsg(StringUtils.substring(e.getMessage(), 0, 2000));
                } else if (e instanceof SQLException) {
                    operLog.setErrorMsg("数据库异常");
                } else {
                    operLog.setErrorMsg("程序异常");
                }
            }
            // 设置方法名称
            String className = joinPoint.getTarget().getClass().getName();
            String methodName = joinPoint.getSignature().getName();
            operLog.setMethod(className + "." + methodName + "()");
            // 设置请求方式
            operLog.setRequestMethod(ServletUtils.getRequest().getMethod());
            // 处理设置注解上的参数
            getControllerMethodDescription(controllerLog, operLog);
            // 异步查询ip地理位置，并保存到数据库
            recordOper(operLog, currentUser);
        } catch (Exception exp) {
            // 记录本地异常日志
            log.error("==前置通知异常==");
            log.error("异常信息:{}", exp.getMessage());
//            exp.printStackTrace();
            log.error(joinPoint.toString());
        }
    }


    @Async
    public void recordOper(SysOperLog operLog, SysUser currentUser) {
        if (StringUtils.isNotNull(currentUser.getDeptId())) {
            SysDept sysDept = sysDeptService.selectDeptById(currentUser.getDeptId());
            operLog.setDeptName(sysDept.getDeptName());
        }

        operLog.setOperLocation(AddressUtils.getRealAddress(operLog.getOperIp()));
        // 保存数据库
        sysOperLogService.insertOperlog(operLog);
    }


    /**
     * 获取注解中对方法的描述信息 用于Controller层注解
     *
     * @param log 切点
     */
    private void getControllerMethodDescription(Log log, SysOperLog operLog) {
        // 设置action动作
        operLog.setBusinessType(log.businessType().ordinal());
        // 设置标题
        operLog.setTitle(log.title());
        // 设置操作人类别
        operLog.setOperatorType(log.operatorType().ordinal());
        // 是否需要保存request，参数和值
        if (log.isSaveRequestData()) {
            // 获取参数的信息，传入到数据库中。
            setRequestValue(operLog);
        }
    }

    /**
     * 获取请求的参数，放到log中
     *
     * @param operLog 操作日志
     */
    private void setRequestValue(SysOperLog operLog) {
        Map<String, String[]> param = ServletUtils.getRequest().getParameterMap();
//        Map<String, String> param = ServletUtils.getAllRequestParam();
        Set<String> keySet = param.keySet();
        for (String key : keySet) {
            if (key != null && key.toLowerCase().contains("password")) {
                param.put(key, new String[]{"******"});
            }
        }
        String params = JSON.toJSON(param).toString();
        operLog.setOperParam(StringUtils.substring(params, 0, 2000));
    }


    /**
     * 是否存在注解，如果存在就获取
     */
    private Log getAnnotationLog(JoinPoint joinPoint) {
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();

        if (method != null) {
            return method.getAnnotation(Log.class);
        }
        return null;
    }
}
