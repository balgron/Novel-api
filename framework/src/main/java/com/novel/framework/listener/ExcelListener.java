package com.novel.framework.listener;

import cn.afterturn.easypoi.cache.manager.POICacheManager;
import com.novel.framework.utils.excel.FileLoaderImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

/**
 * Excel 工具修改文件加载器的监听
 *
 * @author novel
 * @date 2020/3/3
 */
@Component
@Slf4j
public class ExcelListener {
    @EventListener(ApplicationReadyEvent.class)
    public void onApplicationEvent() {
        log.info("POICacheManager init");
        POICacheManager.setFileLoader(new FileLoaderImpl());
    }
}
