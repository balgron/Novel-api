package com.novel.system.domain.vo;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import java.io.Serial;
import java.io.Serializable;

/**
 * 用户导入模板
 *
 * @author novel
 * @date 2020/3/3
 */
@Data
public class SysUserVo implements Serializable {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 登录名称
     */
    @Excel(name = "登录名称")
    private String userName;
    /**
     * 部门编号
     */
    @Excel(name = "部门编号")
    private Long deptId;
    /**
     * 用户姓名
     */
    @Excel(name = "用户姓名")
    private String name;
    /**
     * 邮箱
     */
    @Excel(name = "邮箱", width = 15)
    private String email;
    /**
     * 手机号码
     */
    @Excel(name = "手机号码", width = 15)
    private String phoneNumber;
    /**
     * 用户性别
     */
    @Excel(name = "用户性别", replace = {"男_0", "女_1", "未知_2"})
    private String sex;
    /**
     * 帐号状态
     */
    @Excel(name = "帐号状态", replace = {"正常_0", "停用_1"})
    private String status;
}
