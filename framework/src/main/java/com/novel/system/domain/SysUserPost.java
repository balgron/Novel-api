package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serial;

/**
 * 用户和岗位关联 sys_user_post
 *
 * @author novel
 * @date 2019/6/11
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysUserPost extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 用户ID
     */
    private Long userId;

    /**
     * 岗位ID
     */
    private Long postId;
}
