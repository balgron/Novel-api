package com.novel.system.domain;

import com.novel.framework.base.BaseModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.io.Serial;

/**
 * 用户和角色关联 sys_user_role
 *
 * @author novel
 * @date 2019/4/16
 */
@Data
@ToString
@EqualsAndHashCode(callSuper = false)
public class SysUserRole extends BaseModel {
    @Serial
    private static final long serialVersionUID = 1L;
    /**
     * 用户Id
     */
    private Long userId;

    /**
     * 角色Id
     */
    private Long roleId;
}