package com.novel;

import oshi.SystemInfo;
import oshi.hardware.HardwareAbstractionLayer;

/**
 * @author 李振
 * @date 2022/4/23 0:08
 */
public class Disk {
    public static void main(String[] args) {
        SystemInfo si = new SystemInfo();
        HardwareAbstractionLayer hal = si.getHardware();
        hal.getDiskStores().forEach(d -> {
            System.out.println("Disk Name: " + d);
        });
    }
}
