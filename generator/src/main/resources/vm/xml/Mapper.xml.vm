<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
"http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="${packageName}.mapper.${ClassName}Mapper">
    
    <resultMap type="${packageName}.domain.${ClassName}" id="${ClassName}Result">
#foreach ($column in $columns)
#if($column.columnName == $pkColumn.columnName)
        <id property="${column.javaField}"    column="${column.columnName}"    />
#else
        <result property="${column.javaField}"    column="${column.columnName}"    />
#end
#end
    </resultMap>

    <sql id="Base_Column_List">
#foreach($column in $columns) $column.columnName#if($velocityCount != $columns.size()),#end#end
	</sql>
    <select id="select${ClassName}ById" parameterType="${pkColumn.javaType}" resultMap="${ClassName}Result">
        select
        <include refid="Base_Column_List"/>
        from ${tableName}
        where ${pkColumn.columnName} = #{${pkColumn.javaField}}
    </select>
    
    <select id="select${ClassName}List" parameterType="${packageName}.domain.${ClassName}" resultMap="${ClassName}Result">
        select
        <include refid="Base_Column_List"/>
        from ${tableName}
        <where>  
#foreach($column in $columns)
            <if test="$column.javaField != null#if($column.javaType=='String') and $column.javaField.trim() != ''#end"> and $column.columnName = #{$column.javaField}</if>
#end
        </where>
    </select>
    
    <insert id="insert${ClassName}" parameterType="${packageName}.domain.${ClassName}"#if($pkColumn.increment) useGeneratedKeys="true" keyProperty="$pkColumn.javaField"#end>
        insert into ${tableName}
        <trim prefix="(" suffix=")" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName || !$pkColumn.increment)
			<if test="$column.javaField != null#if($column.javaType=='String') and $column.javaField.trim() != ''#end">$column.columnName,</if>
#end
#end
        </trim>
        <trim prefix="values (" suffix=")" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName || !$pkColumn.increment)
            <if test="$column.javaField != null#if($column.javaType=='String') and $column.javaField.trim() != ''#end">#{$column.javaField},</if>
#end			
#end
        </trim>
    </insert>
	 
    <update id="update${ClassName}ById" parameterType="${packageName}.domain.${ClassName}">
        update ${tableName}
        <trim prefix="SET" suffixOverrides=",">
#foreach($column in $columns)
#if($column.columnName != $pkColumn.columnName)
            <if test="$column.javaField != null#if($column.javaType=='String' && $column.required) and $column.javaField.trim() != ''#end">$column.columnName = #{$column.javaField},</if>
#end
#end
        </trim>
        where ${pkColumn.columnName} = #{${pkColumn.javaField}}
    </update>
	
    <delete id="delete${ClassName}ById" parameterType="${pkColumn.javaType}">
        delete from ${tableName} where ${pkColumn.columnName} = #{${pkColumn.javaField}}
    </delete>
	
    <delete id="batchDelete${ClassName}" parameterType="${pkColumn.javaType}">
        delete from ${tableName} where ${pkColumn.columnName} in
        <foreach item="${pkColumn.javaField}" collection="array" open="(" separator="," close=")">
        #{${pkColumn.javaField}}
        </foreach>
    </delete>
    
</mapper>
