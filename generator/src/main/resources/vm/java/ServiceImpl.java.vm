package ${packageName}.service.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import com.novel.common.utils.StringUtils;
import ${packageName}.mapper.${ClassName}Mapper;
import ${packageName}.domain.${ClassName};
import ${packageName}.service.I${ClassName}Service;

/**
 * ${functionName} 服务层实现
 * 
 * @author ${author}
 * @date ${datetime}
 */
@Service
public class ${ClassName}ServiceImpl implements I${ClassName}Service{

	private final ${ClassName}Mapper ${className}Mapper;

	public ${ClassName}ServiceImpl(${ClassName}Mapper ${className}Mapper) {
		this.${className}Mapper = ${className}Mapper;
	}

	/**
     * 查询${functionName}信息
     *
	 * @param ${pkColumn.javaField} ${functionName}ID
	 * @return ${functionName}信息
     */
	@Override
	public ${ClassName} select${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField}){
	    return ${className}Mapper.select${ClassName}ById(${pkColumn.javaField});
	}
	
	/**
	 * 查询${functionName}列表
	 *
	 * @param ${className} ${functionName}
	 * @return ${functionName}集合
     */
	@Override
	public List<${ClassName}> select${ClassName}List(${ClassName} ${className}){
		return ${className}Mapper.select${ClassName}List(${className});
	}
	
    /**
     * 新增${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	@Override
	public boolean insert${ClassName}(${ClassName} ${className}){
	    return ${className}Mapper.insert${ClassName}(${className}) > 0;
	}
	
	/**
     * 修改${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	@Override
	public boolean update${ClassName}(${ClassName} ${className}){
	    return ${className}Mapper.update${ClassName}ById(${className}) > 0;
	}
	
	/**
     * 保存${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	@Override
	public boolean save${ClassName}(${ClassName} ${className}){
	    ${pkColumn.javaType} ${pkColumn.javaField} = ${className}.get${pkColumn.capJavaField}();
		int rows = 0;
		if (StringUtils.isNotNull(${pkColumn.javaField})){
		    rows = ${className}Mapper.update${ClassName}ById(${className});
		}
		else{
		    rows = ${className}Mapper.insert${ClassName}(${className});
		}
		return rows > 0;
	}
	
	/**
     * 删除${functionName}信息
     * 
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
	@Override
	public boolean delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField}){
	    return ${className}Mapper.delete${ClassName}ById(${pkColumn.javaField}) > 0;
	}
	
	/**
     * 批量删除${functionName}对象
     * 
     * @param ${pkColumn.javaField}s 需要删除的数据ID
     * @return 结果
     */
	@Override
	public boolean batchDelete${ClassName}(${pkColumn.javaType}[] ${pkColumn.javaField}s){
		return ${className}Mapper.batchDelete${ClassName}(${pkColumn.javaField}s) > 0;
	}
	
}
