package ${packageName}.service;

import ${packageName}.domain.${ClassName};
import java.util.List;

/**
 * ${functionName} Service接口
 * 
 * @author ${author}
 * @date ${datetime}
 */
public interface I${ClassName}Service {
	
	/**
     * 查询${functionName}信息
     * 
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return ${functionName}信息
     */
	${ClassName} select${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});
	
	/**
     * 查询${functionName}列表
     * 
     * @param ${className} ${functionName}信息
     * @return ${functionName}集合
     */
	List<${ClassName}> select${ClassName}List(${ClassName} ${className});
	
	/**
     * 新增${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	boolean insert${ClassName}(${ClassName} ${className});
	
	/**
     * 修改${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	boolean update${ClassName}(${ClassName} ${className});
	
	/**
     * 保存${functionName}
     * 
     * @param ${className} ${functionName}信息
     * @return 结果
     */
	boolean save${ClassName}(${ClassName} ${className});
	
	/**
     * 删除${functionName}信息
     * 
     * @param ${pkColumn.javaField} ${functionName}ID
     * @return 结果
     */
	boolean delete${ClassName}ById(${pkColumn.javaType} ${pkColumn.javaField});
	
	/**
     * 批量删除${functionName}信息
     * 
     * @param ${pkColumn.javaField}s 需要删除的${functionName}ID
     * @return 结果
     */
	boolean batchDelete${ClassName}(${pkColumn.javaType}[] ${pkColumn.javaField}s);
	
}
